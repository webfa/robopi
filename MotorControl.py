import RPi.GPIO as GPIO

class MotorControl:
    def __init__(self):
        self.__ENA =
        self.__ENB =
        self.__IN1 =
        self.__IN2 =
        self.__IN3 =
        self.__IN4 =
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(self.__ENA, GPIO.OUT)
        GPIO.setup(self.__ENB, GPIO.OUT)
        GPIO.setup(self.__IN1, GPIO.OUT)
        GPIO.setup(self.__IN2, GPIO.OUT)
        GPIO.setup(self.__IN3, GPIO.OUT)
        GPIO.setup(self.__IN4, GPIO.OUT)

    def __del__(self):
        GPIO.cleanup()

    def forward(self):
        GPIO.output(self.__ENA, GPIO.HIGH)
        GPIO.output(self.__ENB, GPIO.HIGH)
        GPIO.output(self.__IN1, GPIO.HIGH)
        GPIO.output(self.__IN2, GPIO.LOW)
        GPIO.output(self.__IN3, GPIO.HIGH)
        GPIO.output(self.__IN4, GPIO.LOW)

    def backward(self):
        GPIO.output(self.__ENA, GPIO.HIGH)
        GPIO.output(self.__ENB, GPIO.HIGH)
        GPIO.output(self.__IN1, GPIO.LOW)
        GPIO.output(self.__IN2, GPIO.HIGH)
        GPIO.output(self.__IN3, GPIO.LOW)
        GPIO.output(self.__IN4, GPIO.HIGH)

    def right(self):
        GPIO.output(self.__ENA, GPIO.LOW)
        GPIO.output(self.__ENB, GPIO.HIGH)
        GPIO.output(self.__IN1, GPIO.HIGH)
        GPIO.output(self.__IN2, GPIO.LOW)

    def left(self):
        GPIO.output(self.__ENA, GPIO.HIGH)
        GPIO.output(self.__ENB, GPIO.LOW)
        GPIO.output(self.__IN3, GPIO.HIGH)
        GPIO.output(self.__IN4, GPIO.LOW)
        

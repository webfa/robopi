import socket
from MotorControl import MotorControl

class RobotCommunication:
    def __init__(self):
        self._forward = 1
        self._backward = 2
        self._right = 3
        self._left = 4
        self.Motor = MotorControl()

    def __del__(self):
        self.server.close()
        self.client.close()

    def initServer(self):
        self.server = socket.sicket(socket.AF_INET, socket.SOCK_STREAM)
        self.server.bind(("",50000))
        self.server.listen(1)

    def startServer(self):
        while True:
            komm, addr = self.server.accept()
            while True:
                data = komm.recv(1024)
                if not data:
                    komm.close()
                    break

                number = int(data)
                if number == 1:
                    self.Motor.forward()
                elif number == 2:
                    self.Motor.backward()
                elif number == 3:
                    self.Motor.right()
                elif number == 4:
                    self.Motor.left()
                
                komm.send("1")

    def initClient(self, ip, port):
        self.client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.connect((ip,port))

    def sendCommand(self, command):
        self.client.send(str(command))
        self.client.recv(1024)

    
